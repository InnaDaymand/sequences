var arr = [
           [2, 4, 5, 6],
           [7, 1, 8, 9],
           [3, 10, 11, 12]
           ];
var xsize=3;
var ysize=4;

var message='';
var position={x:-1, y:-1};
function findMin(input_array, min){
    position.x=-1;
    position.y=-1;
    for(var i=0; i < input_array.length; i++){
        var row=input_array[i];
        for(var j=0; j< row.length; j++){
            if(row[j]==min){
                position.x=i;
                position.y=j;
                return position;
            }
        }
    }
    return position;
}

var sequences= new Array();
function searchNextNeighbor(cur_min, cur_pos, input_array){
    if((cur_pos.x - 1)>=0 && cur_min+1 == input_array[cur_pos.x-1][cur_pos.y]){
        cur_pos.x=cur_pos.x-1;
        return cur_pos;
    }
    if((cur_pos.x + 1) < xsize && cur_min+1 == input_array[cur_pos.x+1][cur_pos.y]){
        cur_pos.x=cur_pos.x+1;
        return cur_pos;
    }
    if((cur_pos.y - 1)>=0 && cur_min+1 == input_array[cur_pos.x][cur_pos.y-1]){
        cur_pos.y=cur_pos.y-1;
        return cur_pos;
    }
    if((cur_pos.y + 1) < ysize && cur_min+1 == input_array[cur_pos.x][cur_pos.y+1]){
        cur_pos.y=cur_pos.y+1;
        return cur_pos;
    }
    return null;
}

function setSequences(input_array){
    var min=1;
    var min_pos;
    var sequence;
    var cnt=0;
    for(;cnt < xsize*ysize ;){
        if(min ==1 || min_pos==null){
            min_pos=findMin(input_array, min);
            sequence=new Array();
            sequence.push(input_array[min_pos.x][min_pos.y]);
            cnt=cnt+1;
        }
        while(min_pos!=null){
            min_pos=searchNextNeighbor(min, min_pos, input_array);
            if(min_pos!=null){
                sequence.push(input_array[min_pos.x][min_pos.y]);
                cnt=cnt+1;
            }
            min=min+1;
        }
        
        sequences.push(sequence);
        
    }
}

function printSequences(){
    message='';
    for(var i=0; i < sequences.length; i++){
        var seq=sequences[i];
        message+= '<p>'+seq.toString()+'</p>';
    }
    document.getElementById("msg").innerHTML=message;
}

function mySequences(){
    setSequences(arr);
    printSequences();
}



